package com.mzr.springcloud.service;

import com.mzr.springcloud.entities.CommonResult;
import com.mzr.springcloud.entities.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "cloud-provider-payment")//告知去找哪个微服务
public interface PaymentFeignService {
    @GetMapping(value = "/payment/get/{id}")//调用这个微服务中的这个地址
    public CommonResult<Payment> getPaymentById(@PathVariable("id") Long id);

    @GetMapping(value = "/payment/feign/timeout")
    public String paymentFeignTimeout();
}
