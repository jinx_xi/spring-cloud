package com.mzr.springcloud.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//返回给前端通用json字符串
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CommonResult<T> {

    private Integer code;
    private String message;
    private T data;

    //自定义两个参数的构造方法
    public CommonResult(Integer code, String message){
        this(code,message,null);
    }
}
