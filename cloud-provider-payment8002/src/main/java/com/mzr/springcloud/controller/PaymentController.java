package com.mzr.springcloud.controller;

import com.mzr.springcloud.entities.CommonResult;
import com.mzr.springcloud.entities.Payment;
import com.mzr.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @auther zzyy
 * @create 2020-02-18 10:43
 */
@RestController
@Slf4j
public class PaymentController {
    @Resource
    private PaymentService paymentService;

    @Value("${server.port}")
    private String serverPort;

    @PostMapping(value = "/payment/create")
    public CommonResult create(@RequestBody Payment payment) {
        int result = paymentService.create(payment);
        log.info("*****插入结果：" + result);
        return result > 0 ? new CommonResult(200, "插入数据库成功,serverPort" + serverPort, result) : new CommonResult(444, "插入数据库失败,serverPort" + serverPort, null);
    }

    @GetMapping(value = "/payment/get/{id}")
    public CommonResult getPaymentById(@PathVariable("id") Long id) {
        Payment payment = paymentService.getPaymentById(id);
        return ObjectUtils.isEmpty(payment) ? new CommonResult(444, "没有对应记录,查询ID: " + id + "serverPort" + serverPort, null) : new CommonResult(200, "查询成功,serverPort" + serverPort, payment);
    }

    @GetMapping(value = "/payment/lb")
    public String getPaymentLB()
    {
        return serverPort;
    }

}
