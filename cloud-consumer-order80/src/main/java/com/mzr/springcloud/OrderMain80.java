package com.mzr.springcloud;

import com.mzr.myrule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

@SpringBootApplication
@EnableEurekaClient
//在启动微服务时就能够加载我们自定义Ribbon配置类，从而使配置类生效
//name = "CLOUD-PAYMENT-SERVICE"代表当前80微服务是要访问CLOUD-PAYMENT-SERVICE微服务
//configuration= MySelfRule.class代表不使用默认轮询规则，改用自定义随机规则
//@RibbonClient(name = "cloud-provider-payment",configuration= MySelfRule.class)
public class OrderMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderMain80.class, args);
    }
}
