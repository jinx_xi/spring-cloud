package com.mzr.springcloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import javax.xml.ws.Service;
import java.util.List;

public interface LoadBalancer {
    public ServiceInstance instances(List<ServiceInstance> serviceInstances);
}
